Our Ref : 060/RDC-ESSB/84 Cluster (84/60)
 ||[EMPTY_LINE_MARKER]||
 21 August 2020
 ECO SUMMER SDN BHD
 No 9, 11, 15 & 17,
 Jalan EkoPerniagaan 1/6,
 Taman EkoPerniagaan,
 81100, Johor Darul Takzim
 ||[EMPTY_LINE_MARKER]||
 CADANGAN PEMBANGUNAN PERUMAHAN BERSTRATA ‘HOLIDAY HOME RESORT’ UNTUK 84 UNIT RUMAH KLUSTER YANG
 MENGANDUNGI:-
 I) 40 UNIT JENIS RC3-A (33’X75’) DUA TINGKAT DI ATAS SUBLOT 2001-2040
 II) 44 UNIT JENIS RC3-B (33’X75’) DUA TINGKAT DI ATAS SUBLOT 2041-2084
 III) 1 UNIT PENCAWANG ELEKTRIK (DOUBLE CHAMBER) DI ATAS SUBLOT 2404
 DI ATAS SEBAHAGIAN PTD 187017, PLOT 2, TAMAN EKOFLORA, MUKIM TEBRAU, DAERAH JOHOR BAHRU, JOHOR DARUL
 TAKZIM UNTUK TETUAN ECO SUMMER SDN BHD
 ||[EMPTY_LINE_MARKER]||
 ||[EMPTY_LINE_MARKER]||
 ||[EMPTY_LINE_MARKER]||
 Block B1-2 | 2043, 2044, 2065, 2066 (4 units)
 Block B2-3 | 2073, 2074, 2079, 2080 (4 units)
 Block B2-4 | 2075, 2076, 2077, 2078 (4 units)
 ||[EMPTY_LINE_MARKER]||
 ||[EMPTY_LINE_MARKER]||
 PROGRESS OF WORKS CERTIFICATION
 This is to certify that above constructions of the above units have been completed to the following stages marked (X) :
 Stage Description % Completion
 2 Within thirty (30) days after the receipt by the Purchaser of the
 Developer's written notice of the completion of:
 (a) the work below ground level of the said Building comprising the said Parcel 10 PC
 including foundation of the said Building
 (b) the structural framework of the said Parcel 15 PC
 (c) the walls of the said Parcel with door and window frames placed in position 10 PC
 (d) the roofing/ceiling, electrical wiring, plumbing (without fittings), gas piping (if any) and 10 PC
 internal telecommunication trunking and cabling to the said Parcel
 (e) the internal and external finishes of the said Parcel including the wall finishes 10 X
 (f) the sewerage works serving the said Building 5 PC
 (g) the drains serving the said Building 2.5 PC
 (h) the roads serving the said Building 2.5
 3 On the date the Purchaser takes vacant possession of the said Parcel, 17.5
 with water and electricity supply ready for connection
 ||[EMPTY_LINE_MARKER]||
 *PC – Previously Certified
 We hereby certify that all conditions imposed by the Appropriate Authority in respect of the issuance of the
 Certificate of Completion and Compliance have duly complied with.
 ||[EMPTY_LINE_MARKER]||
 RDC Arkitek Sdn Bhd
 ||[EMPTY_LINE_MARKER]||
 ||[EMPTY_LINE_MARKER]||
 ||[EMPTY_LINE_MARKER]||
 ||[EMPTY_LINE_MARKER]||
 _________________________________
 Ar. Tan Choon Kiat
 Lembaga Arkitek Malaysia Reg: A/T 218
 ||[PAGE_END_MARKER]||



