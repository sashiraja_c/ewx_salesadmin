Our Ref : AA/ESSB/TEF/Plot3/Ph1#89dsth/Blk S3/Stg.2g/AC06
 ||[EMPTY_LINE_MARKER]||
 17 November 2020
 ECO SUMMER SDN BHD
 No 9, 11, 15 & 17,
 Jalan EkoPerniagaan 1/6,
 Taman EkoPerniagaan,
 81100, Johor Darul Takzim
 ||[EMPTY_LINE_MARKER]||
 CADANGAN PEMBANGUNAN PERUMAHAN BERSTRATA ‘HOLIDAY HOME RESORT’ YANG MENGANDUNGI:
 I) 89 UNIT RUMAH TERES DUA TINGKAT RT3-B (30’ X 60’)
 II) 1 UNIT PENCAWANG ELKTRIK (DOUBLE CHAMBER)
 DI ATAS SEBAHAGIAN PTD 186994, PLOT 3 (FASA 1), TAMAN EKOFLORA, MUKIM TEBRAU, DAERAH JOHOR BAHRU, JOHOR
 DARUL TAKZIM
 ||[EMPTY_LINE_MARKER]||
 Block S3 | 3177 to 3183 (7 units)
 ||[EMPTY_LINE_MARKER]||
 Property Type | Garden Homes (30' x 60')
 ||[EMPTY_LINE_MARKER]||
 PROGRESS OF WORKS CERTIFICATION
 This is to certify that above constructions of the above units have been completed to the following stages marked (X) :
 Stage Description % Completion
 1 Immediately upon the signing of this Agreement 10 PC
 2 Within thirty (30) days after the receipt by the Purchaser of the
 Developer's written notice of the completion of:
 (a) the work below ground level of the said Building comprising the said Parcel 10 PC
 including foundation of the said Building
 (b) the structural framework of the said Parcel 15 PC
 (c) the walls of the said Parcel with door and window frames placed in position 10 PC
 (d) the roofing/ceiling, electrical wiring, plumbing (without fittings), gas piping (if any) and 10 PC
 internal telecommunication trunking and cabling to the said Parcel
 (e) the internal and external finishes of the said Parcel including the wall finishes 10
 (f) the sewerage works serving the said Building 5 PC
 (g) the drains serving the said Building 2.5 X
 (h) the roads serving the said Building 2.5
 3 On the date the Purchaser takes vacant possession of the said Parcel, 17.5
 with water and electricity supply ready for connection
 ||[EMPTY_LINE_MARKER]||
 *PC – Previously Certified
 We hereby certify that all conditions imposed by the Appropriate Authority in respect of the issuance of the
 Certificate of Completion and Compliance have duly complied with.
 ||[EMPTY_LINE_MARKER]||
 Atria Architects Sdn Bhd
 17/11/2020
 ||[EMPTY_LINE_MARKER]||
 _________________________________
 Ar. Quek Chai Kim
 Lembaga Arkitek Malaysia Reg: A/Q 4
 ||[PAGE_END_MARKER]||



