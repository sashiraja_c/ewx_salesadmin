Our Ref : ANK JB 16/401/(e)/CERT(18/43)
 Date : 21st August 2020
 ||[EMPTY_LINE_MARKER]||
 ||[EMPTY_LINE_MARKER]||
 Eco Summer Sdn Bhd
 The Tomorrow Centre
 No. 9, 11, 15 & 17,
 Jalan Ekoperniagaan 1/6,
 Taman Ekoperniagaan,
 81100 Johor Bahru,
 Johor Darul Takzim
 ||[EMPTY_LINE_MARKER]||
 PROJECT : CADANGAN PEMBANGUNAN PERUMAHAN BERSTRATA ‘HOLIDAY HOME
 RESORT' UNTUK 18 UNIT RUMAH SESEBUAH YANG MENGANDUNGI:
 i) 18 UNIT JENIS RS1 TYPE A (80'x90') DUA TINGKAT DI ATAS SUBLOT 2177 – 2194
 DI ATAS SEBAHAGIAN PTD 187017, PLOT 2, TAMAN EKOFLORA, MUKIM
 TEBRAU, DAERAH JOHOR BAHRU, JOHOR DARUL TAKZIM UNTUK TETUAN
 ECO SUMMER SDN. BHD.
 ||[EMPTY_LINE_MARKER]||
 UNIT : N3 : 2179
 ______________________________________________________________________________
 ||[EMPTY_LINE_MARKER]||
 PROGRESS OF WORKS CERTIFICATION
 ______________________________________________________________________________________
 ||[EMPTY_LINE_MARKER]||
 This is to certify that the construction of the above units have been completed up to the following stages as
 marked ‘(X)’.
 ||[EMPTY_LINE_MARKER]||
 DESCRIPTION % Status
 2 a. The foundation of the said building. 10 X
 b. The structural framework of the said building. 15 X
 c. The walls of the said building with door and window frames placed in X
 10
 position.
 d. The roofing/ceiling, electrical wiring, plumbing (without fittings), gas piping (if X
 10
 any) and internal telephone trunking and cabling (if any) to the said building.
 e. The internal and external finishes of the said building including the wall (X)
 10
 finishes.
 f. The sewerage works serving the said building. 5 X
 g. The drains serving the said building. 2.5 X
 h. The roads serving the said building. 2.5 -
 3. On the date the purchaser takes vacant possession of the said building, with -
 17.5
 water and electricity supply ready for connection.
 ||[EMPTY_LINE_MARKER]||
 ARKITEK N. KANG
 ||[EMPTY_LINE_MARKER]||
 ||[EMPTY_LINE_MARKER]||
 ||[EMPTY_LINE_MARKER]||
 ||[EMPTY_LINE_MARKER]||
 ||[EMPTY_LINE_MARKER]||
 Nicholas Kang
 ||[EMPTY_LINE_MARKER]||
 NOTES :
 ||[EMPTY_LINE_MARKER]||
 1. This Certificate of completion constitutes physical completion by the Contractor of the stage of works as described
 generally above. It does not constitute practical completion of rectification of all defective works by the Contractor.
 2. For item (2d), installation of fittings & fixtures for electrical, water and sanitary works are not completed. The
 Developer undertakes to complete these prior to physical possession by the Purchaser.
 3. X - Certified Previously.
 ||[PAGE_END_MARKER]||



