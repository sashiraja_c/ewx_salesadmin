@echo off
REM --------------------------------------------------------------------
REM 
REM This is a batch file to run AC PDF Parser program for Rumah Mampu Biaya B Project
REM input files.
REM 
REM 
REM 
REM --------------------------------------------------------------------
REM 
set jar_path="C:\Users\muhsin.as\Documents\Automation Anywhere Files\Automation Anywhere\My Tasks\AFX_MasterCode\jar"
set jar_name=Architect-Certificate-Parser.jar
set project_name="rumah_mampu_biaya_b"

echo java -jar %jar_path%\%jar_name% project_name=%project_name%
java -jar %jar_path%\%jar_name% project_name=%project_name%
rem pause