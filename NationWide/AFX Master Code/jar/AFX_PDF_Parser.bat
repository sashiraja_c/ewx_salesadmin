@echo off
REM --------------------------------------------------------------------
REM 
REM This is a batch file to run AFX PDF Parser program
REM 
REM 
REM 
REM --------------------------------------------------------------------
REM 
set jar_path="C:\Users\billy.rpa\Documents\Automation Anywhere Files\Automation Anywhere\My Tasks\AFX Master Code\jar"
set jar_name=Architect-Certificate-Parser.jar
set isProject=true

echo java -jar %jar_path%\%jar_name% IS_PROJECT_MODE=%isProject%
java -jar %jar_path%\%jar_name% IS_PROJECT_MODE=%isProject%
rem pause
